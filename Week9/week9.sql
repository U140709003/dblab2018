select count(customers.customerID) as numberofCustomers,customers.Country
from customers
group by customers.Country
order by numberofCustomers desc;

select count(products.ProductID), suppliers.supplierName
from products join suppliers on products.SupplierID =suppliers.SupplierID
group by products.supplierID
order by count(products.ProductID) desc;

use company;
load data local infile "C:\\Users\\Esra\\Desktop\\DataBase\\week9\\customers.csv"
into table customers
fields terminated by ';'
ignore 1 lines;

select * from customers;
