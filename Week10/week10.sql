SELECT * FROM company.customers;

explain customers;

select *
from customers
order by CustomerName;

select *
from customers
order by CustomerID;

create index abc on customers(CustomerName);

explain customers;

create view myview as
select *
from customers
order by CustomerName;

select *
from myview;

insert into myview
values (10000,"a","a","a","a","a","a");


create view myview3 as
select customers.CustomerID as b,orders.CustomerID as c
from customers,orders
where customers.CustomerID = orders.CustomerID;
